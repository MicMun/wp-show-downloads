<?php

add_action('admin_enqueue_scripts', 'add_admin_css_file_to_plugin');
function add_admin_css_file_to_plugin()
{
  wp_enqueue_style('wpshdwn-admin-style', plugin_dir_url(dirname(__FILE__)) . 'css/plugin-admin-styles.css');
}

add_action('admin_init', 'wpshdown_settings_init');
function wpshdown_settings_init()
{
  register_setting('wpshdown', 'wpshdown_options');

  // add section
  add_settings_section(
    'wpshdown_main_section',
    __('Settings for WP-Show-Downloads.', 'wp-show-downloads'),
    'wpshdown_main_section_cb',
    'wpshdown'
  );

  // register new field for root dir
  add_settings_field(
    'wpshdown_field_root_dir',
    __('Root dir to show files in', 'wp-show-downloads'),
    'wpshdown_field_root_dir_cb',
    'wpshdown',
    'wpshdown_main_section',
    [
      'label_for' => 'wpshdown_field_root_dir'
    ]
  );
}

function wpshdown_main_section_cb($args)
{
  echo "<p id='" . esc_attr($args['id']) . "'>" . __('Set your values.', 'wp-show-downloads') . "</p>";
}

function get_own_options()
{
  $options = get_option(
    'wpshdown_options',
    [
      'wpshdown_field_root_dir' => wp_upload_dir()['basedir']
    ]
  );
  return $options;
}

function wpshdown_field_root_dir_cb()
{
  $options = get_own_options();

  // output the field
  echo "<input id='wpshdown_field_root_dir' name='wpshdown_options[wpshdown_field_root_dir]' 
        size='128' type='text' value='{$options['wpshdown_field_root_dir']}' />";
}

add_action('admin_menu', 'wpshdown_options_page');
function wpshdown_options_page()
{
  // add sub level menu page
  add_menu_page(
    __('WP-Show-Downloads Settings', 'wp-show-downloads'),
    'WP-Show-Downloads',
    'edit_pages',
    'wpshdown',
    'wpshdown_options_page_html'
  );
}

function wpshdown_options_page_html()
{
  // check user capabilities
  if (!current_user_can('edit_pages')) {
    return;
  }

  // check succes and error
  if (isset($_GET['settings-updated'])) {
    add_settings_error(
      'wpshdown_messages',
      'wpshdown_message',
      __('Settings saved', 'wp-show-downloads'),
      'updated'
    );
  }

  // show error/update messages
  settings_errors('wpshdown_messages');
  ?>
  <div class="wrap">
    <h1>
      <?php echo esc_html(get_admin_page_title()); ?>
    </h1>
    <form action="options.php" method="post">
      <?php
      settings_fields('wpshdown');
      do_settings_sections('wpshdown');
      submit_button(__('Save Settings', 'wp-show-downloads'))
        ?>
    </form>
  </div>
  <?php
}
