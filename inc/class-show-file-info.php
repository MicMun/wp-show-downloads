<?php
class ShowFileInfo
{
  private $filename;
  private $lastmodified;
  private $time;
  private $size;
  private $asc;


  public function __construct($filename, $asc = true)
  {
    $this->filename = $filename;
    $this->time = filemtime($filename);
    $this->lastmodified = date('d.m.Y H:i:s', $this->time);
    $this->size = $this->human_filesize(filesize($filename));
    $this->asc = $asc;
  }

  public function getFile()
  {
    return $this->filename;
  }

  public function getLastModified()
  {
    return $this->lastmodified;
  }

  public function getSize()
  {
    return $this->size;
  }

  public function get_download_url($baseurl)
  {
    $pos = strpos($this->filename, 'wp-content');
    $path = substr($this->filename, $pos);
    return $baseurl . '/' . $path;
  }

  public function get_time()
  {
    return $this->time;
  }

  private function human_filesize($bytes, $decimals = 2)
  {
    if (!$bytes)
      return 'NaN';
    $sz = 'BKMGTP';
    $factor = floor((strlen($bytes) - 1) / 3);
    return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
  }
}
