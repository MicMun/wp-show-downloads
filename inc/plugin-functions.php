<?php

function activate_wp_show_downloads()
{
  $htaccess = '# Allow access to some files 
<Files ~ "\.pdf$">
   Order allow,deny
   Allow from all
</Files>

<Files ~ "\.mp3$">
   Order allow,deny
   Allow from all
</Files>';

  $fp = fopen(ABSPATH . '/.htaccess', 'a');
  fwrite($fp, "" . $htaccess . "\n");
  fclose($fp);
}

function deactivate_wp_show_downloads()
{

}

register_activation_hook(__FILE__, 'activate_wp_show_downloads');
register_deactivation_hook(__FILE__, 'deactivate_wp_show_downloads');

add_action('wp_enqueue_scripts', 'add_css_file_to_plugin');
function add_css_file_to_plugin()
{
  wp_enqueue_style('wpshdwn-style', plugin_dir_url(dirname(__FILE__)) . 'css/plugin-styles.css');
}

add_action('init', 'wpshdown_init');
function wpshdown_init()
{
  wpshdown_languages_init();
  wpshdown_shortcodes_init();
}
function wpshdown_languages_init()
{
  $plugin_dir = basename(dirname(dirname(__FILE__)));
  load_plugin_textdomain('wp-show-downloads', false, $plugin_dir . '/languages');
}

function wpshdown_shortcodes_init()
{
  require_once plugin_dir_path(dirname(__FILE__)) . 'inc/class-show-file-info.php';

  function comp_asc($object1, $object2)
  {
    //return $object1->getLastModified() - $object2->getLastmodified();
    return $object1->get_time() - $object2->get_time();
  }

  function comp_desc($object1, $object2)
  {
    return $object2->get_time() - $object1->get_time();
  }

  function get_files($dir, $count, $asc)
  {
    $files = array_diff(scandir($dir), array('..', '.'));
    $fileInfos = array();
    foreach ($files as $f) {
      if (!is_dir($f)) {
        $fileInfos[] = new ShowFileInfo($dir . '/' . $f, $asc);
      }
    }
    if ($asc) {
      usort($fileInfos, 'comp_asc');
    } else {
      usort($fileInfos, 'comp_desc');
    }
    if ($count === -1) {
      $count = count($fileInfos);
    }
    return array_slice($fileInfos, 0, $count);
  }

  function show_files($files, $box = false, $icon = false, $newtab = false)
  {
    $content = '';
    foreach ($files as $file) {
      $content .= '<div';
      if ($box) {
        $content .= ' class="dlbox">';
      } else {
        $content .= ' class="dlbox-no-border">';
      }

      if ($icon) {
        $imgpath = plugin_dir_url(dirname(__FILE__)) . '/assets/sound2.gif';
        $content .= "<img src='$imgpath'> ";
      }
      $target = '';
      if ($newtab) {
        $target = 'target="_blank"';
      }

      $content .= '<a href="' . $file->get_download_url(get_site_url()) . '" ' . $target . '>';
      $content .= basename($file->getFile()) . '</a>';
      $content .= ' (' . $file->getSize() . ')';
      $content .= '</div>';
    }
    return $content;
  }
  function get_navigation($files, $pages, $current)
  {
    global $wp;
    $url = home_url($wp->request);

    $len = count($files);
    $nrOfPages = intval($len / $pages);
    if ($len % $pages !== 0) {
      $nrOfPages++;
    }

    $nav_pages = '';

    if ($current > 1) {
      $nav_pages .= '<a href="' . $url . '?current=' . ($current - 1) . '"><span class="page-nav">&laquo;</span></a>';
    } else {
      $nav_pages .= '<span class="page-nav">&laquo;</span>';
    }

    for ($i = 1; $i <= $nrOfPages; $i++) {
      if ($i === $current) {
        $nav_pages .= '<span class="page-nav">[' . $i . ']</span>';
      } else {
        $nav_pages .= '<a href="' . $url . '?current=' . $i . '"><span class="page-nav">' . $i . '</span></a>';
      }
    }

    if ($current < $nrOfPages) {
      $nav_pages .= '<a href="' . $url . '?current=' . ($current + 1) . '"><span class="page-nav">&raquo;</span></a>';
    } else {
      $nav_pages .= '<span class="page-nav">&raquo;</span>';
    }

    return $nav_pages;
  }

  function wpshdown_shortcode($atts = [], $content = null)
  {
    $options = get_option('wpshdown_options');
    $rootDir = $options['wpshdown_field_root_dir'];
    $a = shortcode_atts(
      array(
        'dir' => '',
        'count' => -1,
        'sort' => 'ASC',
        'box' => false,
        'pages' => 0,
        'icon' => false,
        'newtab' => false
      ),
      $atts
    );
    $currentDir = $rootDir . '/' . $a['dir'];
    $files = get_files($currentDir, $a['count'], $a['sort'] === 'ASC');
    $content = "<div class='display-files'>";
    $pages = intval($a['pages']);

    if ($pages > 0) {
      $current = intval($_GET['current']) ?: 1;
      $nav_pages = get_navigation($files, $pages, $current);

      $pageList = array_slice($files, ($current - 1) * $pages, $pages);
      $content .= show_files($pageList, boolval($a['box']), boolval($a['icon']), boolval($a['newtab']));

      $content .= '<br>' . $nav_pages . '<br>';
    } else {
      $content .= show_files($files, boolval($a['box']), boolval($a['icon']), boolval($a['newtab']));
    }

    $content .= "</div>";

    return $content;
  }

  add_shortcode('show-downloads', 'wpshdown_shortcode');
}
