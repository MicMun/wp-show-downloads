��          t      �                 !     ;     I     Z     z  $   �     �     �     �  �  �     q  )   �     �     �  %   �     �  *        ?     _     q            	      
                                          Michael Munzert Root dir to show files in Save Settings Set your values. Settings for WP-Show-Downloads. Settings saved Shows a number of files to download. WP-Show-Downloads Settings https://micmun.de wp-show-downloads Project-Id-Version: wp-show-downloads 1.0
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/wp-show-downloads
PO-Revision-Date: 2023-09-18 23:51+0200
Last-Translator: Michael Munzert <micmun@micmun.de>
Language-Team: 
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.3.2
X-Domain: wp-show-downloads
 Michael Munzert Wurzelverzeichnis der angezeigten Dateien Einstellungen speichern Setze die Werte. Einstellungen für WP-Show-Downloads. Einstellungen gespeichert Zeigt eine Anzahl an Dateien zum Download. WP-Show-Downloads Einstellungen https://micmun.de wp-show-downloads 