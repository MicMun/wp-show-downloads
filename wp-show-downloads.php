<?php
/**
 * Plugin Name: wp-show-downloads
 * Plugin URI: https://micmun.de
 * Description: Shows a number of files to download.
 * Version: 1.0
 * Author: Michael Munzert
 * Author URI: https://micmun.de
 * License: GPLv3
 * License URI: https://gnu-license.com
 */

if (!defined('WPINC')) {
  die;
}

define('WP_TEST_PLUGIN_VERSION', '1.0');

// plugin-functions.php einbinden.
require_once plugin_dir_path(__FILE__) . 'inc/plugin-functions.php';

// plugin-admin-functions.php einbinden.
require_once plugin_dir_path(__FILE__) . 'inc/plugin-admin-functions.php';
